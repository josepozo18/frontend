export class Salary {
    // Id: number = 0;
    // Year: number = 0;
    // Month: number = 0;
    // OfficeId: number = 0;
    // EmployeeCode: string = '';
    // Name: string = '';
    // Surname: string = '';
    // DivisionId: number = 0;
    // PositionId: number = 0;
    // Grade: number = 0;
    // BeginDate: any;
    // BirthDay: any;
    // IdentificationNumber: string = '';
    // BaseSalary: number = 0;
    // ProductionBonus: number = 0;
    // CompensationBonus: number = 0;
    // Comission: number = 0;
    // Contributions: number = 0;

    id: number = 0;
    year: number = 0;
    month: number = 0;
    officeId: number = 0;
    employeeCode: string = '';
    name: string = '';
    surname: string = '';
    divisionId: number = 0;
    positionId: number = 0;
    grade: number = 0;
    beginDate: any;
    birthDay: any;
    identificationNumber: string = '';
    baseSalary: number = 0;
    productionBonus: number = 0;
    compensationBonus: number = 0;
    comission: number = 0;
    contributions: number = 0;

    // constructor(_Id: number, _Year: number, _Month: number,
    //             _OfficeId: number, _EmployeeCode: string, _Name: string,
    //             _Surname: string, _DivisionId: number, _PositionId: number,
    //             _Grade: number, _BeginDate: Date, _BirthDay: Date,
    //             _IdentificationNumber: string, _BaseSalary: number, _ProductionBonus: number,
    //             _CompensationBonus: number, _Comission: number, _Contributions: number){

    //     this.Id=_Id;
    //     this.Year=_Year;
    //     this.Month=_Month;
    //     this.OfficeId=_OfficeId;
    //     this.EmployeeCode=_EmployeeCode;
    //     this.Name=_Name;
    //     this.Surname=_Surname;
    //     this.DivisionId=_DivisionId;
    //     this.PositionId=_PositionId;
    //     this.Grade=_Grade;
    //     this.BeginDate=_BeginDate;
    //     this.BirthDay=_BirthDay;
    //     this.IdentificationNumber=_IdentificationNumber;
    //     this.BaseSalary=_BaseSalary;
    //     this.ProductionBonus=_ProductionBonus;
    //     this.CompensationBonus=_CompensationBonus;
    //     this.Comission=_Comission;
    //     this.Contributions=_Contributions;

    // }

}