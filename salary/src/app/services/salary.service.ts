import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Helper } from "../Helpers/helper.model";

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};

@Injectable({
    providedIn: 'root'
})
export class SalaryService {

    private readonly baseUrl = 'https://localhost:44398/api/';
    
    //helper = new Helper();

    constructor(private HttpClient: HttpClient) { }

    // Salary
    postSalary(listDataSalary: any[]): Observable<any[]> {
        return this.HttpClient.post<any[]>(this.baseUrl + 'Salary', listDataSalary, httpOptions);
    }

    putSalary(listDataSalary: any[]): Observable<any[]> {
        return this.HttpClient.put<any[]>(this.baseUrl + 'Salary', listDataSalary, httpOptions);
    }

    getAllSalary(): Observable<any[]> {
        return this.HttpClient.get<any[]>(this.baseUrl + 'Salary', httpOptions);
    }

    getSalaryByEmployeeCode(employeeCode: string): Observable<any[]> {
        return this.HttpClient.get<any[]>(this.baseUrl + 'Salary/' + employeeCode, httpOptions);
    }

    getSalaryById(id: number): Observable<any> {
        return this.HttpClient.get<any>(this.baseUrl + 'Salary/api/Salary/GetByPK/' + id,  httpOptions);
    }

    // Office
    getAllOffice(): Observable<any[]> {
        return this.HttpClient.get<any[]>(this.baseUrl + 'Office', httpOptions);
    }

    // Position
    getAllPostion(): Observable<any[]> {
        return this.HttpClient.get<any[]>(this.baseUrl + 'Position', httpOptions);
    }

    // Division
    getAllDivision(): Observable<any[]> {
        return this.HttpClient.get<any[]>(this.baseUrl + 'Division', httpOptions);
    }

}