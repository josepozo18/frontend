import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrincipalComponent } from './Components/principal/principal.component';
import { EmployeeComponent } from './Components/employee/employee.component';
import { routing } from './app.routing';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SalaryService } from './services/salary.service';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [SalaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
