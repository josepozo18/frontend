import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import { PrincipalComponent } from './Components/principal/principal.component';
import { EmployeeComponent } from './Components/employee/employee.component';

const app_routes: Routes = [
    { path: 'principal', component: PrincipalComponent },
    { path: 'employee', component: EmployeeComponent },
    { path: 'employee/:employeeCode', component: EmployeeComponent },
    { path: '', component: PrincipalComponent }
]

export const routing = RouterModule.forRoot(app_routes);