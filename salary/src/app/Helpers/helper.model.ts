import { Salary } from "../shared/salary.model";

export class Helper {
    dataFormSalary = new Salary();
    listDataSalary: Salary[] = [];

    AddDataSalary(dataForm: Salary) {
        //let objSalary = new Salary();
        //objSalary = dataForm;
        this.listDataSalary.push({...dataForm});

        this.listDataSalary.map(data => {
            if (data.employeeCode == dataForm.employeeCode) {
                data.name = dataForm.name;
                data.surname = dataForm.surname;
                data.birthDay = dataForm.birthDay;
                data.employeeCode = dataForm.employeeCode;
                data.identificationNumber = dataForm.identificationNumber;
            }
        });

        // this.dataFormSalary.grade = 0;
        // this.dataFormSalary.baseSalary = 0;
        // this.dataFormSalary.year = 0;
        // this.dataFormSalary.month = 0;
        // this.dataFormSalary.beginDate = null;
        // this.dataFormSalary.productionBonus = 0;
        // this.dataFormSalary.compensationBonus = 0;
        // this.dataFormSalary.comission = 0;
        // this.dataFormSalary.contributions = 0;
    }

    AddDataEditSalary(dataForm: Salary) {
        //let objSalary = new Salary();
        //objSalary = dataForm;
        let index = this.listDataSalary.findIndex(data => data.id == dataForm.id);
        this.listDataSalary.splice(index, 1, dataForm);

        //console.log(dataForm);

        this.listDataSalary.map(data => {
            if (data.employeeCode == dataForm.employeeCode) {
                data.name = dataForm.name;
                data.surname = dataForm.surname;
                data.birthDay = dataForm.birthDay;
                data.employeeCode = dataForm.employeeCode;
                data.identificationNumber = dataForm.identificationNumber;
            }
        });

        // this.dataFormSalary.grade = 0;
        // this.dataFormSalary.baseSalary = 0;
        // this.dataFormSalary.year = 0;
        // this.dataFormSalary.month = 0;
        // this.dataFormSalary.beginDate = null;
        // this.dataFormSalary.productionBonus = 0;
        // this.dataFormSalary.compensationBonus = 0;
        // this.dataFormSalary.comission = 0;
        // this.dataFormSalary.contributions = 0;
    }

}