import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { Helper } from 'src/app/Helpers/helper.model';
import { SalaryService } from 'src/app/services/salary.service';
import { Salary } from 'src/app/shared/salary.model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  offices: any[] = [];
  positions: any[] = [];
  divisions: any[] = [];

  listDataSalary: any[] = [];
  dataFormSalary: any[] = [];

  helper = new Helper();

  visibilityAdd: string = "";
  visibilityEdit: string = "hidden";

  messajeError: string = "";

  showOfficeById(officeId:number): string {
    if (officeId == undefined || officeId == 0) return "";
    let item = this.offices.find(data => data.id == officeId);
    return item.nombre;
  }

  showDivisionById(divisionId:number): string {
    if (divisionId == undefined ||  divisionId == 0) return "";
    let item = this.divisions.find(data => data.id == divisionId);
    return item.nombre;
  }

  editData(id:number): void {
    this.visibilityAdd = "hidden";
    this.visibilityEdit = "";
    this.service.getSalaryById(id).subscribe(data => {
      this.helper.dataFormSalary = data;
    })
  }

  deleteData(index:number): void {
    this.helper.listDataSalary.splice(index,1);
  }
 
  constructor(
    public service: SalaryService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.loadDataOffice();
    this.loadDataPosition();
    this.loadDataDivision();
    this.employeeSelected();
  }

  private employeeSelected(): void {
    let parms: any;
    this.route.paramMap.subscribe(params => {
      parms = params.get('employeeCode')?.toString();
    });

    if(parms == undefined ) {
      //this.service.helper.listDataSalary = [];
      //this.service.helper.dataFormSalary = new Salary();
      return;
    }

    if(parms.length>0) {
      this.service.getSalaryByEmployeeCode(parms)
        .subscribe(data => {
          //this.helper.listDataSalary = [];
          //this.helper.dataFormSalary = new Salary();
          data.map(d => {
            this.helper.dataFormSalary.name = d.name;
            this.helper.dataFormSalary.surname = d.surname;
            this.helper.dataFormSalary.employeeCode = d.employeeCode;
            this.helper.dataFormSalary.identificationNumber = d.identificationNumber;
            this.helper.dataFormSalary.officeId = d.officeId;
            this.helper.dataFormSalary.positionId = d.positionId;
            this.helper.dataFormSalary.divisionId = d.divisionId;
            this.helper.dataFormSalary.birthDay = d.birthDay;
            this.helper.AddDataSalary(d);
          })
      });
    }
    //else {
      //this.helper.listDataSalary = [];
      //this.service.helper.dataFormSalary = new Salary();
    //}
    
  }

  private loadDataOffice(): void {
    this.service.getAllOffice().subscribe(data => {
      this.offices = data;
    });
  }

  private loadDataPosition(): void {
    this.service.getAllPostion().subscribe(data => {
      this.positions = data;
    });
  }

  private loadDataDivision(): void {
    this.service.getAllDivision().subscribe(data => {
      this.divisions = data;
    });
  }

  SaveData(): void {
    this.service.putSalary(this.helper.listDataSalary).subscribe(
      res => console.log(res),
      err => console.log(err)
    );
    this.router.navigate(['/']);
  }

  SaveEdit(): void {
    this.service.putSalary(this.helper.listDataSalary).subscribe(
      res =>  console.log(res),
      err => console.log(err)
    );
    this.router.navigate(['/']);
  }
  
}
