import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SalaryService } from 'src/app/services/salary.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  public showSalary : any[] = [];
  classSelected: string = "";
  rowSelected: any = {};

  constructor(
    private servicio: SalaryService,
    private router: Router) { }
  
  ngOnInit(): void {
    this.getData();
  }

  private getData(): void {
    this.servicio.getAllSalary().subscribe(data => {
      this.showSalary = data;
    });
  }

  // Marcar registro seleccionado por el usuario
  rowSelection(item: {}): void {
    if(Object.keys(item).length ===0 ) return;
    this.rowSelected = item;
    this.classSelected = "rowSelected";
  }

  // Editar el registro seleccionado
  editData(item : any): void {
    if(Object.keys(item).length ===0 ) return;
    this.router.navigate(['/employee', item.employeeCode]);
  }

  // Sin filtro
  filter0(): void {
    this.getData();
  }

  // Empleados con la misma Oficina y Grado.
  filter1(item: any): void {
    if(Object.keys(item).length ===0 ) return;
    this.showSalary = [];
    this.servicio.getAllSalary().subscribe(data => {
      for (let index = 0; index < data.length; index++) {
        if ((item.officeId == data[index].officeId) && (item.grade == data[index].grade)){
          this.showSalary.push({...data[index]})
        }        
      }
    });
  }

  // Empleados de todas las Oficinas y con el mismo Grado.
  filter2(item: any): void {
    if(Object.keys(item).length ===0 ) return;
    this.showSalary = [];
    this.servicio.getAllSalary().subscribe(data => {
      for (let index = 0; index < data.length; index++) {
        if (item.grade == data[index].grade){
          this.showSalary.push({...data[index]})
        }        
      }
    });
  }

  // Empleados con la misma Posición y Grado.
  filter3(item: any): void {
    if(Object.keys(item).length ===0 ) return;
    this.showSalary = [];
    this.servicio.getAllSalary().subscribe(data => {
      for (let index = 0; index < data.length; index++) {
        if ((item.positionId == data[index].positionId) && (item.grade == data[index].grade)){
          this.showSalary.push({...data[index]})
        }        
      }
    });
  }

  // Empleados de todas las Posiciones y con el mismo Grado.
  filter4(item: any): void {
    if(Object.keys(item).length ===0 ) return;
    this.showSalary = [];
    this.servicio.getAllSalary().subscribe(data => {
      for (let index = 0; index < data.length; index++) {
        if (item.grade == data[index].grade){
          this.showSalary.push({...data[index]})
        }        
      }
    });
  }

}
